# project_0 - An app mainly built to communicate with ESP32 BLE server to get data from SHT30 module.

## how to just test the app?

1. Install flutter: [https://flutter.dev/docs/get-started/install](https://flutter.dev/docs/get-started/install)
2. Clone repo
3. In folder type ```flutter run``` (you need working AVD or your mobile phone connected through USB)

## how to build APK
1. Ho to folder and type ```flutter build apk```
2. Copy apk to your device storage
3. Install and enjoy :)