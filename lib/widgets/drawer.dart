import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:project_0/screens/compass/compass.dart';
import 'package:project_0/screens/login/login.dart';
import 'package:project_0/screens/profile/profile.dart';
import 'package:project_0/screens/settings/settings.dart';
import 'package:project_0/screens/temperature/temperature.dart';
import 'package:project_0/helperFunctions.dart';

class AppDrawer extends StatefulWidget {
  final String userEmail;
  final bool isConnected;
  final BluetoothDevice device;

  const AppDrawer({this.userEmail, this.isConnected, this.device});

  @override
  AppDrawerState createState() => AppDrawerState();
}

class AppDrawerState extends State<AppDrawer> {
  BluetoothDevice _device = null;
  bool _isConnected = false;

  @override
  Widget build(BuildContext context) {
    setState(() {
      _device = widget.device;
      widget.isConnected != null
          ? _isConnected = widget.isConnected
          : _isConnected = false;
    });
    print('ApDrawe: ${widget.userEmail}');
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
              child: Column(children: [
            CircleAvatar(
              backgroundColor: Colors.blue,
            ),
            Text('${widget.userEmail}',
                style: Theme.of(context).textTheme.headline2)
          ])),
          populateListTile(
              'Profile',
              Theme.of(context).textTheme.headline3,
              () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProfileScreen(
                            userEmail: widget.userEmail,
                            isConnected: widget.isConnected,
                            device: _device,
                          ))),
              Icons.face,
              Theme.of(context).iconTheme),
          populateListTile(
              'Settings',
              Theme.of(context).textTheme.headline3,
              () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SettingsScreen(
                            userEmail: widget.userEmail,
                            isConnected: widget.isConnected,
                            device: _device,
                          ))),
              Icons.settings,
              Theme.of(context).iconTheme),
          populateListTile(
              'Temperature',
              Theme.of(context).textTheme.headline3,
              !_isConnected
                  ? null
                  : () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TemperatureScreen(
                                userEmail: widget.userEmail,
                                isConnected: widget.isConnected,
                                device: _device,
                              ))),
              Icons.hot_tub,
              Theme.of(context).iconTheme),
          populateListTile(
              'Compass',
              Theme.of(context).textTheme.headline3,
              () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CompassScreen(
                            userEmail: widget.userEmail,
                            isConnected: widget.isConnected,
                            device: _device,
                          ))),
              Icons.compass_calibration,
              Theme.of(context).iconTheme),
          Expanded(child: Container()),
          populateListTile('Logout', Theme.of(context).textTheme.headline3, () {
            _device != null ? _device.disconnect() : _device = null;
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => new LoginForm()));
          }, Icons.logout, Theme.of(context).iconTheme),
        ],
      ),
    );
  }
}
