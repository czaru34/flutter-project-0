import 'package:flutter/material.dart';

class MyPasswordFormWidget extends StatefulWidget {
  final String labelText;
  final Function validator;
  final Function onSaved;

  MyPasswordFormWidget({this.labelText, this.validator, this.onSaved});

  @override
  _MyPasswordFormState createState() => _MyPasswordFormState();
}

class _MyPasswordFormState extends State<MyPasswordFormWidget> {
  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: TextFormField(
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle1,
          decoration: InputDecoration(
              labelText: widget.labelText,
              prefixIcon: Icon(Icons.security),
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() => this._showPassword = !this._showPassword);
                },
                icon: Icon(Icons.remove_red_eye,
                    color: this._showPassword ? Colors.pink : Colors.white),
              )),
          obscureText: !this._showPassword,
          validator: widget.validator,
          onSaved: widget.onSaved,
          keyboardType: TextInputType.text,
        ));
  }
}
