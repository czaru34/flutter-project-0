import 'package:flutter/material.dart';

class ApplicationBar extends StatelessWidget implements PreferredSizeWidget {
  const ApplicationBar({Key key, this.title, this.userEmail, this.arrowButton})
      : super(key: key);
  final String title;
  final String userEmail;
  final bool arrowButton;

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title: Text(title),
        automaticallyImplyLeading: true,
        leading: arrowButton
            ? IconButton(
                icon: Icon(Icons.arrow_back),
                color: Theme.of(context).accentColor,
                onPressed: () => Navigator.pop(context, [userEmail]),
              )
            : null);
  }
}
