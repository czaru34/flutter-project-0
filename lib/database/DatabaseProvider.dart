import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import '../models/user.dart';

class DatabaseProvider {
  static final DatabaseProvider _instance = new DatabaseProvider.internal();
  factory DatabaseProvider() => _instance;
  DatabaseProvider.internal();

  static final String tableName = "Users";
  static final String columnUserId = 'id';
  static final String columnUserEmail = "email";
  static final String columnUserPassword = "password";

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDatabase();
    return _database;
  }

  initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "myappdatabase.db");
    return await openDatabase(path,
        version: 1, onOpen: (db) {}, onCreate: _onCreate);
  }

  void _onCreate(Database db, int version) async {
    String sql = "CREATE TABLE Users ("
        "id INTEGER PRIMARY KEY,"
        "$columnUserEmail TEXT UNIQUE,"
        "$columnUserPassword TEXT,"
        "isLoggedIn TEXT"
        ")";
    await db.execute(sql);
  }

  Future<int> saveUser(User user) async {
    var dbClient = await database;
    print(user.email);
    int result = await dbClient.insert("Users", user.toMap());
    String sql = "Select * from $tableName";
    List<Map> list = await dbClient.rawQuery(sql);
    print(list);
    return result;
  }

  Future<User> selectUser(User user) async {
    print("Select User");
    print(user.email + " " + user.password);
    var dbClient = await database;
    List<Map> usermaps = await dbClient.query(tableName,
        columns: [columnUserEmail, columnUserPassword],
        where: "$columnUserEmail = ? and $columnUserPassword = ?",
        whereArgs: [user.email, user.password]);
    print(usermaps);
    if (usermaps.length > 0) {
      print("User recognized by email");
      return user;
    } else {
      return null;
    }
  }

  Future<int> selectUserId(User user) async {
    var dbClient = await database;
    List<Map> usermaps = await dbClient.query(tableName,
        columns: [columnUserId, columnUserEmail],
        where: "$columnUserEmail = ?",
        whereArgs: [user.email]);
    if (usermaps.length > 0) {
      print('Found user id: ${usermaps.first['id']}');
      return usermaps.first['id'];
    } else {
      return null;
    }
  }
}
