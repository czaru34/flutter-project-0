import 'dart:async';
import 'package:project_0/models/user.dart';
import 'package:project_0/database/DatabaseProvider.dart';

class RestData {
  Future<User> login(String email, String password) async {
    String isLoggedIn = "logged";
    var user = new User(email, password, null);

    var db = new DatabaseProvider();
    var userRetorno = new User(null, null, null);
    userRetorno = await db.selectUser(user);

    if (userRetorno != null) {
      isLoggedIn = "logged";
      return new Future.value(new User(email, password, isLoggedIn));
    } else {
      isLoggedIn = "not";
      return new Future.value(new User(email, password, isLoggedIn));
    }
  }
}
