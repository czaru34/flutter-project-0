import 'package:flutter/material.dart';

Widget populateListTile(String title, TextStyle style, GestureTapCallback onTap,
    [IconData icon, IconThemeData iconThemeData]) {
  return ListTile(
    title: Text(
      title,
      style: style,
    ),
    leading: (iconThemeData != null)
        ? Icon(
            icon,
            color: iconThemeData.color,
          )
        : Icon(icon),
    onTap: onTap,
  );
}
