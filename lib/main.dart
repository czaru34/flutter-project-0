import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:project_0/screens/login/register.dart';
import 'package:project_0/screens/mainScreen/main.dart';
import 'screens/login/login.dart';
import 'package:camera/camera.dart';
import 'package:project_0/globals.dart';

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

Future<void> main() async {
  runApp(MyApp());
  cameras = await availableCameras();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primaryColor: Color.fromRGBO(255, 20, 147, 1),
          accentColor: Color.fromRGBO(100, 49, 115, 1),
          textTheme: TextTheme(
            headline1: TextStyle(
                color: Color.fromRGBO(255, 20, 147, 0.75),
                fontSize: 50,
                fontWeight: FontWeight.bold),
            headline2: TextStyle(
              color: Color.fromRGBO(255, 20, 147, 0.75),
              fontSize: 30,
            ),
            headline3: TextStyle(
              color: Color.fromRGBO(255, 20, 147, 0.75),
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
            headline4: TextStyle(
                color: Colors.white54,
                fontSize: 50,
                fontWeight: FontWeight.bold),
            headline5: TextStyle(
                color: Colors.white30,
                fontSize: 30,
                fontWeight: FontWeight.bold),
            subtitle1: TextStyle(color: Color.fromRGBO(255, 182, 193, 1)),
            subtitle2: TextStyle(
              color: Color.fromRGBO(255, 20, 147, 0.75),
              fontSize: 10,
            ),
          ),
          cardTheme: CardTheme(
            shadowColor: Color.fromRGBO(255, 20, 147, 1),
            color: Color.fromRGBO(100, 49, 115, 1),
          ),
          backgroundColor: Color.fromRGBO(1, 22, 39, 1),
          cursorColor: Color.fromRGBO(65, 234, 212, 1),
          scaffoldBackgroundColor: Color.fromRGBO(1, 22, 39, 1),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          iconTheme: IconThemeData(color: Color.fromRGBO(100, 49, 115, 1)),
          dialogBackgroundColor: Color.fromRGBO(1, 22, 39, 1),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: TextButton.styleFrom(
                primary: Colors.white, backgroundColor: Colors.pink),
          ),
          canvasColor: Color.fromRGBO(1, 22, 39, 1),
          inputDecorationTheme: InputDecorationTheme(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.lightBlue)),
            focusedBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.pink)),
            hintStyle: TextStyle(color: Color.fromRGBO(255, 182, 193, 1)),
            labelStyle: TextStyle(color: Colors.white),
            focusColor: Color.fromRGBO(255, 20, 147, 0.25),
            errorStyle: TextStyle(color: Colors.red),
          )),
      initialRoute: '/login',
      routes: {
        '/login': (context) => LoginForm(),
        '/register': (context) => RegisterForm(),
        '/main': (context) => MainScreen()
      },
    );
  }
}
