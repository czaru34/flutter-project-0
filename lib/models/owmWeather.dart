class Weather {
  final int _temperature;
  final int _humidity;
  final String _name;

  Weather(this._temperature, this._humidity, this._name);

  int get temperature => _temperature;
  int get humidity => _humidity;
  String get name => _name;

  factory Weather.fromJSON(Map<String, dynamic> json) {
    return Weather(
        json['main']['temp'], json['main']['humidity'], json['name']);
  }
}
