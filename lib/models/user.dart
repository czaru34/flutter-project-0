class User {
  String _email;
  String _password;
  String _isLoggedIn;

  User(this._email, [this._password, this._isLoggedIn]);

  User.fromMap(dynamic obj) {
    this._email = obj['email'];
    this._password = obj['password'];
    this._isLoggedIn = obj['isLoggedIn'];
  }

  String get email => _email;
  String get password => _password;
  String get isLoggedIn => _isLoggedIn;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["email"] = _email;
    map["password"] = _password;
    map['isLoggedIn'] = _isLoggedIn;
    return map;
  }
}
