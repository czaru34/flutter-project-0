class Device {
  int _userid;
  String _ipaddress;

  Device(this._userid, this._ipaddress);

  Device.fromMap(dynamic obj) {
    this._userid = obj['userid'];
    this._ipaddress = obj['ipaddress'];
  }

  int get userid => _userid;
  String get ipaddress => _ipaddress;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['userid'] = _userid;
    map['ipaddress'] = _ipaddress;
    return map;
  }
}
