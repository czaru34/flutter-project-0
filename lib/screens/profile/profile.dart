import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:project_0/screens/profile/lastPicture.dart';
import 'package:project_0/widgets/appbar.dart';
import 'package:project_0/widgets/drawer.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:project_0/globals.dart';
import 'package:path_provider/path_provider.dart';
import 'package:project_0/database/DatabaseProvider.dart';

class ProfileScreen extends StatefulWidget {
  final String userEmail;
  final bool isConnected;
  final BluetoothDevice device;
  ProfileScreen({this.userEmail, this.isConnected, this.device});
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with WidgetsBindingObserver {
  CameraController _controller;
  String _userEmail;
  String _lastPath;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _controller = CameraController(cameras[0], ResolutionPreset.ultraHigh);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (_controller == null || !_controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      _controller?.dispose();
    }
  }

  Future<String> _takePicture() async {
    if (!_controller.value.isInitialized) {
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (_controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await _controller.takePicture(filePath);
      Fluttertoast.showToast(msg: 'Saved as ${filePath}');
      setState(() {
        _lastPath = filePath;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LastPictureScreen(
                    _lastPath,
                    userEmail: widget.userEmail,
                    isConnected: widget.isConnected,
                    device: widget.device,
                  )));
    } on CameraException catch (e) {
      print(e);
      return null;
    }
    return filePath;
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
  @override
  Widget build(BuildContext context) {
    setState(() {
      _userEmail = widget.userEmail;
    });
    print(widget.userEmail);
    return Scaffold(
        appBar: ApplicationBar(
          arrowButton: true,
          title: 'Picture',
          userEmail: widget.userEmail,
        ),
        drawer: AppDrawer(
            device: widget.device,
            isConnected: widget.isConnected,
            userEmail: widget.userEmail),
        body: Center(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              !_controller.value.isInitialized
                  ? Text(
                      'waiting?',
                      style: Theme.of(context).textTheme.headline4,
                    )
                  : Expanded(
                      flex: 1,
                      child: AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        child: CameraPreview(_controller),
                      )),
              ElevatedButton(
                  onPressed: () => _takePicture(), child: Text('TAKE A SHOT'))
            ])));
  }
}
