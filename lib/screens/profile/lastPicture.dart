import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:project_0/widgets/appbar.dart';
import 'package:project_0/widgets/drawer.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:project_0/globals.dart';
import 'package:path_provider/path_provider.dart';
import 'package:project_0/database/DatabaseProvider.dart';

class LastPictureScreen extends StatefulWidget {
  final String userEmail;
  final bool isConnected;
  final BluetoothDevice device;
  final String lastPath;
  LastPictureScreen(this.lastPath,
      {this.userEmail, this.isConnected, this.device});
  @override
  _LastPictureScreenState createState() => _LastPictureScreenState();
}

class _LastPictureScreenState extends State<LastPictureScreen>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
  @override
  Widget build(BuildContext context) {
    print(widget.userEmail);
    return Scaffold(
        appBar: ApplicationBar(
          arrowButton: true,
          title: widget.lastPath,
          userEmail: widget.userEmail,
        ),
        drawer: AppDrawer(
            device: widget.device,
            isConnected: widget.isConnected,
            userEmail: widget.userEmail),
        body: Center(
          child: Column(children: <Widget>[
            Expanded(
                flex: 1,
                child: widget.lastPath != null
                    ? new Image.file(
                        new File(widget.lastPath),
                      )
                    : CircularProgressIndicator()),
          ]),
        ));
  }
}
