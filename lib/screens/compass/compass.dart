import 'package:compasstools/compasstools.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter/material.dart';
import 'package:project_0/widgets/appbar.dart';

class CompassScreen extends StatefulWidget {
  static const String routeName = '/compass';
  final String userEmail;
  final BluetoothDevice device;
  final bool isConnected;

  const CompassScreen({this.userEmail, this.device, this.isConnected});

  @override
  _CompassScreenState createState() => _CompassScreenState();
}

class _CompassScreenState extends State<CompassScreen> {
  int _haveSensor;
  String sensorType;

  @override
  void initState() {
    super.initState();
    checkDeviceSensors();
  }

  Future<void> checkDeviceSensors() async {
    int haveSensor;

    try {
      haveSensor = await Compasstools.checkSensors;

      switch (haveSensor) {
        case 0:
          {
            // statements;
            sensorType = "No sensors for Compass";
          }
          break;

        case 1:
          {
            //statements;
            sensorType = "Accelerometer + Magnetoneter";
          }
          break;

        case 2:
          {
            //statements;
            sensorType = "Gyroscope";
          }
          break;

        default:
          {
            //statements;
            sensorType = "Error!";
          }
          break;
      }
    } on Exception {}

    if (!mounted) return;

    setState(() {
      _haveSensor = haveSensor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ApplicationBar(
        arrowButton: true,
        title: 'Compass',
        userEmail: widget.userEmail,
      ),
      body: new Container(
        child: Column(
          children: <Widget>[
            StreamBuilder(
              stream: Compasstools.azimuthStream,
              builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                if (snapshot.hasData) {
                  return Column(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Text(
                        'Heading: ${snapshot.data}°',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    ),
                    Container(
                      child: new RotationTransition(
                        turns: new AlwaysStoppedAnimation(-snapshot.data / 360),
                        child: Image.asset("assets/compass.png"),
                      ),
                    ),
                  ]);
                } else
                  return Text(
                    "Error in stream",
                    style: TextStyle(color: Colors.red),
                  );
              },
            ),
          ],
        ),
      ),
    );
  }
}
