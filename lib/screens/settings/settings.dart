import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:project_0/screens/mainScreen/main.dart';
import 'package:project_0/widgets/drawer.dart';
import 'package:project_0/screens/settings/bluetooth.dart';
import 'package:project_0/helperFunctions.dart';

class SettingsScreen extends StatefulWidget {
  static const String routeName = '/settings';
  final String userEmail;
  final bool isConnected;
  final BluetoothDevice device;

  const SettingsScreen({this.userEmail, this.isConnected, this.device});

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  String _userEmail = '';
  bool _isConnected;
  BluetoothDevice _device = null;
  set isConnected(bool value) => setState(() => _isConnected = value);

  @override
  Widget build(BuildContext context) {
    setState(() {
      _userEmail = widget.userEmail;
      _isConnected = widget.isConnected;
      _device = widget.device;
    });
    return Scaffold(
        appBar: AppBar(
            title: Text('Settings'),
            automaticallyImplyLeading: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Theme.of(context).accentColor,
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MainScreen(
                            userEmail: _userEmail,
                            isConnected: _isConnected,
                            device: _device,
                          ))),
            )),
        drawer: AppDrawer(
          userEmail: _userEmail,
          isConnected: _isConnected,
          device: _device,
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                populateListTile(
                    'Connect to device via Bluetooth',
                    Theme.of(context).textTheme.headline3,
                    () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BluetoothScreen(
                                isConnected: _isConnected,
                                userEmail: _userEmail))),
                    widget.isConnected
                        ? Icons.bluetooth_connected_sharp
                        : Icons.bluetooth_disabled_sharp,
                    IconThemeData(
                        color: _isConnected
                            ? Colors.lightGreen
                            : Theme.of(context).accentColor))
              ],
            )));
  }
}
