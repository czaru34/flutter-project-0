import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:project_0/screens/settings/settings.dart';
import 'package:project_0/widgets/drawer.dart';
import 'package:flutter_blue/flutter_blue.dart' as BLuE;

class BluetoothScreen extends StatefulWidget {
  static const String routeName = '/bluetooth';
  final String userEmail;
  final bool isConnected;
  const BluetoothScreen({this.userEmail, this.isConnected});

  @override
  _BluetoothScreenState createState() => _BluetoothScreenState();
}

class _BluetoothScreenState extends State<BluetoothScreen> {
  final String _deviceName = 'T&H BLE SERVER';
  final String _serviceUuid = 'a949afda-366e-11eb-adc1-0242ac120002';
  final String _characteristicUuid = 'b98a8586-366e-11eb-adc1-0242ac120002';
  bool _isConnected;
  BLuE.BluetoothDevice _device;

  _onTap(BLuE.BluetoothDevice device) async {
    if (device.name == _deviceName) {
      try {
        await device.connect();
      } catch (e) {
        if (e.code != 'already_connected') {
          throw e;
        } else
          Fluttertoast.showToast(msg: 'You have already connected ;)');
      } finally {
        List<BLuE.BluetoothService> services = await device.discoverServices();
        var compare = _handleUuidComparison(services);
        if (compare) {
          _device = device;
          print(_device);
          setState(() {
            _isConnected = true;
          });
          Fluttertoast.showToast(
              msg: 'Now you can launch Temperature from Drawer!');
        } else {
          device.disconnect();
          Fluttertoast.showToast(
              msg: 'I still think it is not our device. Disconnecting... :\'(');
        }
      }
    }
  }

  bool _handleUuidComparison(List<BLuE.BluetoothService> services) {
    for (BLuE.BluetoothService service in services) {
      print('Service UUID: ${service.uuid.toString()}');
      if (service.uuid.toString() == _serviceUuid) {
        List<BLuE.BluetoothCharacteristic> characteristics =
            service.characteristics;
        for (BLuE.BluetoothCharacteristic characteristic in characteristics)
          if (characteristic.uuid.toString() == _characteristicUuid)
            return true;
      }
    }
    print('_handleUuidComparison: false');
    return false;
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isConnected = widget.isConnected;
    });
    _device != null ? _device.connect() : null;
    return Scaffold(
      appBar: AppBar(
          title: Text('Connect to device via bluetooth'),
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Theme.of(context).accentColor,
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SettingsScreen(
                          userEmail: widget.userEmail,
                          isConnected: true,
                          device: _device,
                        ))),
          )),
      drawer: AppDrawer(
        userEmail: widget.userEmail,
        isConnected: _isConnected,
        device: _device,
      ),
      body: StreamBuilder<BLuE.BluetoothState>(
        stream: BLuE.FlutterBlue.instance.state,
        initialData: BLuE.BluetoothState.unknown,
        builder: (context, snapshot) {
          final state = snapshot.data;
          if (state == BLuE.BluetoothState.on)
            return RefreshIndicator(
              color: Theme.of(context).primaryColor,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    StreamBuilder<List<BLuE.ScanResult>>(
                      stream: BLuE.FlutterBlue.instance.scanResults,
                      initialData: [],
                      builder: (context, snapshot) => Column(
                        children: snapshot.data
                            .map((result) => ListTile(
                                leading: Icon(
                                  _isConnected &&
                                          result.device.name == _deviceName
                                      ? Icons.bluetooth_connected
                                      : Icons.bluetooth,
                                  color: _isConnected &&
                                          result.device.name == _deviceName
                                      ? Colors.lightGreen
                                      : Theme.of(context).accentColor,
                                ),
                                title: Text(
                                  result.device.name.isNotEmpty
                                      ? result.advertisementData.localName
                                      : 'Not our device',
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                                subtitle: Text(
                                  result.device.id.toString(),
                                  style: Theme.of(context).textTheme.subtitle2,
                                ),
                                onTap: result.device.name != ''
                                    ? () => _onTap(result.device)
                                    : () => Fluttertoast.showToast(
                                        msg:
                                            'I think it is not our device :(')))
                            .toList(),
                      ),
                    )
                  ],
                ),
              ),
              onRefresh: () => BLuE.FlutterBlue.instance
                  .startScan(timeout: Duration(seconds: 4)),
            );
          else
            return Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                'Bluetooth Adapter is ${state != null ? state.toString().substring(15) : 'not available'}',
                style: Theme.of(context).textTheme.headline2,
              ),
            );
        },
      ),
      floatingActionButton: StreamBuilder<bool>(
        stream: BLuE.FlutterBlue.instance.isScanning,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: () => BLuE.FlutterBlue.instance.stopScan(),
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
                child: Icon(Icons.search),
                onPressed: () => BLuE.FlutterBlue.instance
                    .startScan(timeout: Duration(seconds: 4)));
          }
        },
      ),
    );
  }
}
