import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:project_0/widgets/appbar.dart';
import 'package:project_0/widgets/drawer.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:project_0/models/owmWeather.dart';

class TemperatureScreen extends StatefulWidget {
  static const String routeName = '/charts';
  final String userEmail;
  final BluetoothDevice device;
  final bool isConnected;

  const TemperatureScreen({this.userEmail, this.device, this.isConnected});

  @override
  _TemperatureScreenState createState() => _TemperatureScreenState();
}

class _TemperatureScreenState extends State<TemperatureScreen> {
  String _userEmail = '';
  bool _isConnected;
  bool _isReady;
  BluetoothDevice _device = null;
  final String _owmApiKey = '4c1af16cd7ba485acbfa8b82cb028089';
  final String _serviceUuid = 'a949afda-366e-11eb-adc1-0242ac120002';
  final String _characteristicUuid = 'b98a8586-366e-11eb-adc1-0242ac120002';
  Stream<List<int>> _stream = null;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  Weather _weather;
  Timer _apiTimer;
  Timer _disconnected;
  bool _show;

  @override
  void initState() {
    super.initState();
    _isReady = false;
    _show = false;
    checkConnection();
    _apiTimer = new Timer(Duration(seconds: 9), () => _getCurrentLocation());
  }

  @override
  void dispose() {
    super.dispose();
    _apiTimer.cancel();
    _disconnected.cancel();
  }

  checkConnection() async {
    if (widget.device == null) {
      print('Device is null. Popping.');
      Navigator.of(context).pop(true);
    }
    new Timer(const Duration(seconds: 20), () {
      if (!_isConnected) {
        print('Not connected. Popping :/');
        widget.device.disconnect();
        Navigator.of(context).pop(true);
      }
    });
    getStream();
  }

  getStream() async {
    if (widget.device == null) {
      Navigator.of(context).pop(true);
      return;
    }
    List<BluetoothService> services = await widget.device.discoverServices();
    services.forEach((service) {
      if (service.uuid.toString() == _serviceUuid) {
        print('sermvice');
        service.characteristics.forEach((characteristic) {
          if (characteristic.uuid.toString() == _characteristicUuid) {
            characteristic.setNotifyValue(!characteristic.isNotifying);

            setState(() {
              _isReady = true;
              _stream = characteristic.value;
            });
          }
        });
      }
      setState(() {
        _isReady = false;
      });
    });
  }

  _getCurrentLocation() async {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _callOwmApi();
    }).catchError((e) {
      print(e);
    });
  }

  _callOwmApi() async {
    try {
      final apiResponse = await fetchWeather();
      print(apiResponse.body);
      if (apiResponse.statusCode == 200) {
        print('Response code: 200');
        setState(() {
          _weather = Weather.fromJSON(jsonDecode(apiResponse.body));
        });
        print(_weather.temperature);
      } else {
        throw Exception('Failed to fetch from OWM Api.');
      }
      setState(() {});
    } catch (e) {
      print(e);
    }
  }

  Future<http.Response> fetchWeather() {
    return http.get(
        'https://api.openweathermap.org/data/2.5/weather?lat=${_currentPosition.latitude}&lon=${_currentPosition.longitude}&appid=${_owmApiKey}&units=metric');
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isConnected = widget.isConnected;
    });
    return Scaffold(
        appBar: ApplicationBar(
          title: 'Temperature & Humidity',
          userEmail: widget.userEmail,
          arrowButton: true,
        ),
        drawer: AppDrawer(userEmail: _userEmail, isConnected: _isConnected),
        body: Container(
            child: ListView(children: <Widget>[
          Container(
              child: Card(
                  child: Container(
            alignment: Alignment.center,
            child: _stream == null
                ? Text('Stream is null')
                : new StreamBuilder<List<int>>(
                    initialData: [],
                    stream: _stream,
                    builder: (BuildContext context,
                        AsyncSnapshot<List<int>> snapshot) {
                      _isReady = !_isReady;
                      if (snapshot.connectionState == ConnectionState.active &&
                          snapshot.hasData &&
                          snapshot.data != null &&
                          _isReady) {
                        var currentData = utf8.decode(snapshot.data);
                        String temperature = currentData.split(',')[0];
                        String humidity = currentData.split(',')[1];
                        Text(
                          'Temperature: ${temperature} °C',
                          style: Theme.of(context).textTheme.headline2,
                        );
                        _isReady = false;
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Inside:',
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Text(
                              'Temperature: ${temperature} °C',
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            Text(
                              'Humidity: ${humidity} %',
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ],
                        );
                      }
                      print(_isReady);
                      return Text('hemlo');
                    },
                  ),
          ))),
          Container(
            child: Card(
              child: Column(
                children: [
                  Text(
                    _weather != null ? _weather.name : 'Outside:',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: _weather != null
                        ? Text(
                            'Temperature: ${_weather.temperature} °C',
                            style: Theme.of(context).textTheme.headline5,
                          )
                        : CircularProgressIndicator(
                            backgroundColor: Colors.amber,
                          ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: _weather != null
                        ? Text(
                            'Humidity: ${_weather.humidity} %',
                            style: Theme.of(context).textTheme.headline5,
                          )
                        : CircularProgressIndicator(
                            backgroundColor: Colors.amber,
                          ),
                  )
                ],
              ),
            ),
          )
        ])));
  }
}
