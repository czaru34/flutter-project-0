import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:project_0/widgets/appbar.dart';
import 'package:project_0/widgets/drawer.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class MainScreen extends StatefulWidget {
  final String userEmail;
  final bool isConnected;
  final BluetoothDevice device;
  const MainScreen({this.userEmail, this.isConnected, this.device});

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ApplicationBar(
          title: 'Hemlo',
          userEmail: widget.userEmail,
          arrowButton: false,
        ),
        drawer: AppDrawer(
          userEmail: widget.userEmail,
          isConnected: widget.isConnected,
          device: widget.device,
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              Container(
                  alignment: Alignment.center,
                  child: Text(
                    'This app can communicate with ESP32 through BLE, show temperature and humidity, take photos and save them.',
                    style: Theme.of(context).textTheme.headline2,
                    textAlign: TextAlign.center,
                  )),
              Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Swipe right to configure',
                    style: Theme.of(context).textTheme.headline2,
                    textAlign: TextAlign.center,
                  )),
            ],
          ),
        ));
  }
}
