import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:project_0/models/user.dart';
import 'package:project_0/screens/login/loginPresenter.dart';
import 'package:project_0/screens/login/register.dart';
import 'package:project_0/screens/mainScreen/main.dart';
import 'package:project_0/widgets/MyPasswordFormWidget.dart';
import 'package:project_0/widgets/MyTextFormField.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> implements LoginFormContract {
  final _formKey = GlobalKey<FormState>();
  String _email = '', _password = '';
  bool _isConnected = false;

  LoginFormPresenter _presenter;

  _LoginFormState() {
    _presenter = new LoginFormPresenter(this);
  }

  void _submit() {
    final form = _formKey.currentState;

    if (form.validate()) {
      setState(() {
        form.save();
        _presenter.logIn(_email, _password);
      });
    }
  }

  @override
  void onLoginSuccess(User user) async {
    if (user.isLoggedIn == 'logged') {
      Fluttertoast.showToast(msg: 'Logged in.');
      await Future.delayed(Duration(seconds: 1), () {
        FocusScope.of(context).unfocus();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MainScreen(
                    userEmail: user.email, isConnected: _isConnected)));
      });
    } else {
      Fluttertoast.showToast(msg: 'No such account.');
    }
  }

  @override
  void onLoginError(String error) async {
    Fluttertoast.showToast(msg: 'Cannot log in. Check credentials.');
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _email = '';
      _isConnected = false;
    });
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.all(10),
            child: Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'The Best App',
                          style: Theme.of(context).textTheme.headline1,
                        )),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      child: MyTextFormField(
                        onSaved: (val) => _email = val,
                        labelText: 'e-mail',
                        isEmail: true,
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Enter e-mail!';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: Column(children: [
                          MyPasswordFormWidget(
                            onSaved: (val) => _password = val,
                            labelText: 'password',
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Enter password!';
                              }
                              return null;
                            },
                          ),
                        ])),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: ElevatedButton(
                          child: Text('Log in'),
                          autofocus: true,
                          onPressed: _submit,
                        )),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(20),
                      child: RichText(
                          text: TextSpan(children: [
                        new TextSpan(
                          text: 'No account? ',
                        ),
                        new TextSpan(
                            text: 'Register.',
                            style: TextStyle(color: Colors.lightBlue),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => RegisterForm()));
                              })
                      ])),
                    )
                  ],
                ))));
  }
}
