import 'package:flutter/material.dart';
import 'package:project_0/database/DatabaseProvider.dart';
import 'package:project_0/models/user.dart';
import 'package:project_0/screens/login/login.dart';
import 'package:project_0/widgets/MyPasswordFormWidget.dart';
import 'package:project_0/widgets/MyTextFormField.dart';
import 'package:project_0/widgets/appbar.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();
  String _email, _password;

  String _validateEmail(String value) {
    if (value.isEmpty)
      return 'Enter email';
    else {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Enter Valid Email.';
      else
        return null;
    }
  }

  String _validatePassword(String value) {
    if (value.isEmpty)
      return 'Enter password';
    else {
      Pattern pattern =
          r"^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!\u0022#$%&'()*+,./:;<=>?@[\]\^_`{|}~-]).*)";
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Must contain 1 Uppercase, lowercase, 1 numerical and special character.';
      else
        return null;
    }
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      var user = new User(_email, _password, null);
      var database = new DatabaseProvider();
      database.saveUser(user);

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginForm()));
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ApplicationBar(
          title: 'Hemlo',
          arrowButton: false,
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Register',
                          style: Theme.of(context).textTheme.headline1,
                        )),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: MyTextFormField(
                            onSaved: (value) => _email = value,
                            labelText: 'e-mail',
                            isEmail: true,
                            validator: (String value) =>
                                _validateEmail(value))),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: MyPasswordFormWidget(
                            onSaved: (value) => _password = value,
                            labelText: 'password',
                            validator: (String value) =>
                                _validatePassword(value))),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: ElevatedButton(
                          child: Text('Register'),
                          autofocus: true,
                          onPressed: _submit,
                        )),
                  ],
                ))));
  }
}
