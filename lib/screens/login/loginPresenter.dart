import 'package:project_0/database/r.dart';
import 'package:project_0/models/user.dart';

abstract class LoginFormContract {
  void onLoginSuccess(User user);
  void onLoginError(String error);
}

class LoginFormPresenter {
  LoginFormContract _view;
  RestData api = new RestData();
  LoginFormPresenter(this._view);

  logIn(String email, String password) {
    api
        .login(email, password)
        .then((user) => _view.onLoginSuccess(user))
        .catchError((onError) => _view.onLoginError(onError));
  }
}
